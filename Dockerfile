FROM opsperator/php

# ServiceDesk image for OpenShift Origin

ARG DO_UPGRADE=
ENV LTBSD_VERSION=0.4 \
    LTBSD_REPO=https://ltb-project.org/debian/stable/pool/main

LABEL io.k8s.description="LTB ServiceDesk allows for administrators to manage LDAP accounts" \
      io.k8s.display-name="ServiceDesk $LTBSD_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="servicedesk,ltb-servicedesk" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-servicedesk" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$LTBSD_VERSION"

USER root

COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install ServiceDesk Dependencies" \
    && if test `uname -m` = aarch64; then \
	sed 's|amd64|arm64|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:arm64.list; \
	done; \
    elif test `uname -m` = armv7l; then \
	sed 's|amd64|armhf|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:armhf.list; \
	done; \
    else \
	cat /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-gd php-ldap php; do \
	    echo "/." >/var/lib/dpkg/info/$dep:amd64.list; \
	done; \
    fi \
    && apt-get -y install libpng16-16 ldap-utils openssl libjpeg62-turbo \
	libwebp6 libgd3 libxpm4 libtiff5 libfreetype6 libfontconfig1 \
	smarty3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install libicu-dev libpng-dev libldap2-dev gnupg2 \
	apt-transport-https libjpeg62-turbo-dev libfreetype6-dev libxml2-dev \
	libonig-dev libwebp-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
	--with-png \
    && docker-php-ext-install mbstring intl gd ldap xml \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && echo "# Install ServiceDesk" \
    && curl -fsSL http://ltb-project.org/wiki/lib/RPM-GPG-KEY-LTB-project \
	| apt-key add - \
    && mv /ltb-project.list /etc/apt/sources.list.d/ \
    && apt-get update \
    && if ! apt install -y service-desk; then \
	echo WARNING: could not fetch service-desk using apt - fallback to curl \
	&& curl -fsSL -o /tmp/service-desk.deb \
	    $LTBSD_REPO/l/ltb-project-service-desk/service-desk_${LTBSD_VERSION}-1_all.deb \
	&& dpkg -i /tmp/service-desk.deb; \
    fi \
    && mv /*.jpg /*.png /usr/share/service-desk/htdocs/images/ \
    && echo "# Enabling LDAP Modules" \
    && a2enmod ldap authnz_ldap \
    && echo "# Fixing permissions" \
    && for dir in /etc/lemonldap-ng /usr/share/service-desk/conf \
	/var/cache/service-desk /var/cache/service-desk/cache \
	/var/cache/service-desk/templates_c; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning up" \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/etc/apache2/sites-available/service-desk.conf \
	/etc/apache2/sites-enabled/service-desk.conf /dpkg-patch \
	/usr/src/php.tar.xz \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-servicedesk.sh"]
USER 1001

# k8s ServiceDesk

LTB ServiceDesk image.

Based on https://ltb-project.org/documentation/service-desk

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description                      | Default                                                     | Inherited From    |
| :-------------------------- | ----------------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`            | ServiceDesk ServerName              | `servicedesk.${OPENLDAP_DOMAIN}`                            | opsperator/apache |
|  `APACHE_HTTP_PORT`         | ServiceDesk HTTP(s) Port            | `8080`                                                      | opsperator/apache |
|  `AUTH_METHOD`              | Apache Auth Method                  | `lemon`, could be `lemon`, `ldap`, `oidc` or `none`         | opsperator/apache |
|  `BACKGROUND_IMAGE`         | ServiceDesk Background Image        | `wall_dark.jpg` (should be in config/ building image)       |                   |
|  `IDLE_DAYS`                | ServiceDesk IDLE Threshold          | `60` days                                                   |                   |
|  `LANG`                     | ServiceDesk Language                | `en`                                                        |                   |
|  `LDAP_START_TLS`           | OpenLDAP uses StartTLS              | false                                                       |                   |
|  `LOGO_IMAGE`               | ServiceDesk Logo Image              | `kubelemon.png` (should be in config/ building image)       |                   |
|  `OIDC_CALLBACK_URL`        | OpenID Callback Path                | `/oauth2/callback`                                          |                   |
|  `OIDC_CLIENT_ID`           | OpenID Client ID                    | `changeme`                                                  |                   |
|  `OIDC_CLIENT_SECRET`       | OpenID Client Secret                | `secret`                                                    |                   |
|  `OIDC_CRYPTO_SECRET`       | OpenID Crypto Secret                | `secret`                                                    |                   |
|  `OIDC_META_URL`            | OpenID Meta Path                    | `/.well-known/openid-configuration`                         |                   |
|  `OIDC_PORTAL`              | OpenID Portal                       | `https://auth.$OPENLDAP_DOMAIN`                             |                   |
|  `OIDC_SKIP_TLS_VERIFY`     | OpenID Skip TLS Verify              | undef                                                       |                   |
|  `OIDC_TOKEN_ENDPOINT_AUTH` | OpenID Auth Method                  | `client_secret_basic`                                       |                   |
|  `ONLY_TRUST_KUBE_CA`       | Don't trust base image CAs          | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`            | OpenLDAP Base                       | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX`  | OpenLDAP Bind DN Prefix             | `cn=servicedesk,ou=services`                                | opsperator/apache |
|  `OPENLDAP_BIND_PW`         | OpenLDAP Bind Password              | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX`  | OpenLDAP Conf DN Prefix             | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`          | OpenLDAP Domain Name                | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_FILTER`          | OpenLDAP Search Filters             | `(&(memberOf=cn=Admins,ou=groups,$OPENLDAP_BASE)(active))`  |                   |
|  `OPENLDAP_HOST`            | OpenLDAP Backend Address            | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`            | OpenLDAP Bind Port                  | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`           | OpenLDAP Proto                      | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`           | PHP Errors Logs Output              | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`   | PHP Max Execution Time              | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`     | PHP Max File Uploads                | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`        | PHP Max Post Size                   | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE`  | PHP Max Upload File Size            | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`         | PHP Memory Limit                    | `-1` (no limitation)                                        | opsperator/php    |
|  `PING_PATH`                | Proxy healthcheck url               | undef - if set, `/PING_PATH` bypasses auth                  |                   |
|  `PING_ROOT`                | Proxy healthcheck docroot           | `/var/www/html` - alternate DocumentRoot for health checks  |                   |
|  `PUBLIC_PROTO`             | Apache Public Proto                 | `http`                                                      | opsperator/apache |
|  `USE_CHECKPASSWORD`        | Enables Password Check              | `true`                                                      |                   |
|  `USE_LOCKACCOUNT`          | Enables Account Lock                | `true`                                                      |                   |
|  `USE_RESETPASSWORD`        | Enables Password Reset              | `true`                                                      |                   |
|  `USE_SEARCHEXPIRED`        | Enables Expired Dashboard           | `true`                                                      |                   |
|  `USE_SEARCHIDLE`           | Enables IDLE Dashboard              | `true`                                                      |                   |
|  `USE_SEARCHLOCKED`         | Enables Locked Dashboard            | `true`                                                      |                   |
|  `USE_SEARCHWILLEXPIRE`     | Enables Expiring-Soon Dashboard     | `true`                                                      |                   |
|  `USE_UNLOCKACCOUNT`        | Enables Account Unlock              | `true`                                                      |                   |
|  `WILLEXPIRE_DAYS`          | ServiceDesk Expiring-Soon Threshold | `14` days                                                   |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |

apiVersion: v1
kind: Template
labels:
  app: servicedesk
  template: servicedesk-ephemeral
message: |-
  The following service(s) have been created in your project:
      https://servicedesk.${ROOT_DOMAIN} -- ServiceDesk

  Global admin username: demoone
  Built-in users : demotoo & demotri
  and password: see openldap-${FRONTNAME} secret
metadata:
  annotations:
    description: ServiceDesk - ephemeral
    iconClass: icon-php
    openshift.io/display-name: ServiceDesk
    tags: servicedesk
  name: servicedesk-ephemeral
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: openldap-${FRONTNAME}
    name: openldap-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: openldap-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: openldap-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: OPENLDAP_BIND_LDAP_PORT
            value: "1389"
          - name: OPENLDAP_BIND_LDAPS_PORT
            value: "1636"
          - name: OPENLDAP_BLUEMIND_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: bluemind-password
          - name: OPENLDAP_DEBUG_LEVEL
            value: "${OPENLDAP_DEBUG_LEVEL}"
          - name: OPENLDAP_DEMO_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: demo-password
          - name: OPENLDAP_FUSION_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: fusion-password
          - name: OPENLDAP_HOST_ENDPOINT
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_LEMONLDAP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-password
          - name: OPENLDAP_LEMONLDAP_HTTPS
            value: "yes"
          - name: OPENLDAP_LEMONLDAP_SESSIONS_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: lemonldap-sessions-password
          - name: OPENLDAP_LEMON_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_MEDIAWIKI_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: mediawiki-password
          - name: OPENLDAP_MONITOR_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: monitor-password
          - name: OPENLDAP_NEXTCLOUD_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: nextcloud-password
          - name: OPENLDAP_ORG_SHORT
            value: "${ORG_NAME}"
          - name: OPENLDAP_ROCKET_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: rocket-password
          - name: OPENLDAP_ROOT_DN_PREFIX
            value: cn=admin
          - name: OPENLDAP_ROOT_DN_SUFFIX
            value: "${BASE_SUFFIX}"
          - name: OPENLDAP_ROOT_DOMAIN
            value: "${ROOT_DOMAIN}"
          - name: OPENLDAP_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: root-password
          - name: OPENLDAP_SMTP_SERVER
            value: "${SMTP_RELAY}"
          - name: OPENLDAP_SSO_CLIENT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: ssoapp-password
          - name: OPENLDAP_SERVICEDESK_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: servicedesk-password
          - name: OPENLDAP_SYNCREPL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: syncrepl-password
          - name: OPENLDAP_WHITEPAGES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: whitepages-password
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            timeoutSeconds: 1
            tcpSocket:
              port: 1389
          name: openldap
          ports:
          - containerPort: 1389
            protocol: TCP
          - containerPort: 1636
            protocol: TCP
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /usr/local/bin/is-ready.sh
            initialDelaySeconds: 5
            timeoutSeconds: 1
          resources:
            limits:
              cpu: "${OPENLDAP_CPU_LIMIT}"
              memory: "${OPENLDAP_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - name: conf
            mountPath: /etc/ldap
          - name: data
            mountPath: /var/lib/ldap
          - name: run
            mountPath: /run
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: conf
        - emptyDir: {}
          name: data
        - emptyDir: {}
          name: run
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - openldap
        from:
          kind: ImageStreamTag
          name: ${OPENLDAP_IMAGESTREAM_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      template.openshift.io/expose-uri: ldaps://{.spec.clusterIP}:{.spec.ports[?(.name=="ldaps")].port}
    name: openldap-${FRONTNAME}
  spec:
    ports:
    - name: ldap
      protocol: TCP
      port: 1389
      targetPort: 1389
      nodePort: 0
    - name: ldaps
      protocol: TCP
      port: 1636
      targetPort: 1636
      nodePort: 0
    selector:
      name: openldap-${FRONTNAME}
    type: ClusterIP
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: servicedesk-${FRONTNAME}
    name: servicedesk-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: servicedesk-${FRONTNAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          name: servicedesk-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - capabilities: {}
          env:
          - name: SERVICEDESK_DOMAIN
            value: servicedesk.${ROOT_DOMAIN}
          - name: SERVICEDESK_HTTP_PORT
            value: "8080"
          - name: OPENLDAP_BASE
            value: ${BASE_SUFFIX}
          - name: OPENLDAP_BIND_DN_PREFIX
            value: cn=servicedesk,ou=services
          - name: OPENLDAP_BIND_PW
            valueFrom:
              secretKeyRef:
                name: openldap-${FRONTNAME}
                key: servicedesk-password
          - name: OPENLDAP_DOMAIN
            value: ${ROOT_DOMAIN}
          - name: OPENLDAP_HOST
            value: openldap-${FRONTNAME}
          - name: OPENLDAP_PORT
            value: "1389"
          - name: OPENLDAP_PROTO
            value: ldap
          image: ' '
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 8
          name: servicedesk
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            initialDelaySeconds: 5
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${SERVICEDESK_CPU_LIMIT}"
              memory: "${SERVICEDESK_MEMORY_LIMIT}"
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - mountPath: /etc/apache2/sites-enabled
            name: apachesites
            subPath: apache
          - mountPath: /var/cache/service-desk/templates_c
            name: apachesites
            subPath: smarty
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: apachesites
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - servicedesk
        from:
          kind: ImageStreamTag
          name: servicedesk:${SERVICEDESK_IMAGE_TAG}
        lastTriggeredImage: ''
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: servicedesk-${FRONTNAME}
  spec:
    ports:
    - name: http
      port: 8080
      targetPort: 8080
    selector:
      name: servicedesk-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: servicedesk-${FRONTNAME}
  spec:
    host: servicedesk.${ROOT_DOMAIN}
    to:
      kind: Service
      name: servicedesk-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: OPENLDAP_CPU_LIMIT
  description: Maximum amount of CPU an OpenLDAP container can use
  displayName: OpenLDAP CPU Limit
  required: true
  value: 300m
- name: OPENLDAP_DEBUG_LEVEL
  description: OpenLDAP log level
  displayName: LDAP Log Level
  required: true
  value: '256'
- name: OPENLDAP_IMAGESTREAM_TAG
  description: OpenLDAP Image Tag
  displayName: OpenLDAP ImageStream Tag
  required: true
  value: openldap:master
- name: OPENLDAP_MEMORY_LIMIT
  description: Maximum amount of memory an OpenLDAP container can use
  displayName: OpenLDAP Memory Limit
  required: true
  value: 512Mi
- name: SERVICEDESK_CPU_LIMIT
  description: Maximum amount of CPU a SERVICEDESK container can use
  displayName: SERVICEDESK CPU Limit
  required: true
  value: 300m
- name: SERVICEDESK_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: SERVICEDESK_MEMORY_LIMIT
  description: Maximum amount of memory a SERVICEDESK container can use
  displayName: SERVICEDESK Memory Limit
  required: true
  value: 512Mi
- name: BASE_SUFFIX
  description: OpenLDAP base suffix
  displayName: LDAP Base Suffix
  required: true
  value: dc=demo,dc=local
- name: ORG_NAME
  description: Organization Display Name
  displayName: Organization Display Name
  required: true
  value: Demo
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local

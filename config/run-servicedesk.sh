#!/bin/sh

if test "$DEBUG"; then
    DEBUG_SD=true
    set -x
else
    DEBUG_SD=false
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
AUTH_METHOD=${AUTH_METHOD:-lemon}
BACKGROUND_IMAGE=${BACKGROUND_IMAGE:-wall_dark.jpg}
IDLE_DAYS=${IDLE_DAYS:-60}
LANG=${LANG:-en}
LOGO_IMAGE=${LOGO_IMAGE:-kubelemon.png}
OIDC_CALLBACK_URL=${OIDC_CALLBACK_URL:-/oauth2/callback}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-changeme}
OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET:-secret}
OIDC_CRYPTO_SECRET=${OIDC_CRYPTO_SECRET:-secret}
OIDC_META_URL=${OIDC_META_URL:-/.well-known/openid-configuration}
OIDC_TOKEN_ENDPOINT_AUTH=${OIDC_TOKEN_ENDPOINT_AUTH:-client_secret_basic}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=servicedesk,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-127.0.0.1}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PING_PATH=${PING_PATH:-}
PING_ROOT=${PING_ROOT:-/var/www/html}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
USE_CHECKPASSWORD=${USE_CHECKPASSWORD:-true}
USE_LOCKACCOUNT=${USE_LOCKACCOUNT:-true}
USE_RESETPASSWORD=${USE_RESETPASSWORD:-true}
USE_SEARCHEXPIRED=${USE_SEARCHEXPIRED:-true}
USE_SEARCHIDLE=${USE_SEARCHIDLE:-true}
USE_SEARCHLOCKED=${USE_SEARCHLOCKED:-true}
USE_SEARCHWILLEXPIRE=${USE_SEARCHWILLEXPIRE:-true}
USE_UNLOCKACCOUNT=${USE_UNLOCKACCOUNT:-true}
WILLEXPIRE_DAYS=${WILLEXPIRE_DAYS:-14}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=servicedesk.$OPENLDAP_DOMAIN
fi
if test "$AUTH_METHOD" = ldap; then
    if test -z "$OPENLDAP_FILTER"; then
	OPENLDAP_FILTER="(&(memberOf=cn=Admins,ou=groups,$OPENLDAP_BASE)(!(pwdAccountLockedTime=*)))"
    fi
    OPENLDAP_SEARCH="ou=users,$OPENLDAP_BASE?uid?sub?$OPENLDAP_FILTER"
elif test "$AUTH_METHOD" = oidc -a -z "$OIDC_PORTAL"; then
    OIDC_PORTAL=$PUBLIC_PROTO://auth.$OPENLDAP_DOMAIN
fi
if test "$OPENLDAP_PROTO" = ldaps; then
    LDAP_START_TLS=false
elif test "$LDAP_START_TLS"; then
    LDAP_START_TLS=true
else
    LDAP_START_TLS=false
fi
if ! test -s "/usr/share/service-desk/htdocs/images/$BACKGROUND_IMAGE"; then
    BACKGROUND_IMAGE=wall_dark.jpg
fi
if ! test -s "/usr/share/service-desk/htdocs/images/$LOGO_IMAGE"; then
    LOGO_IMAGE=kubelemon.png
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if ! test -s /usr/share/service-desk/conf/config.inc.local.php; then
    echo Install ServiceDesk Custom Site Configuration
    sed -e "s|BACKGROUND_IMAGE|$BACKGROUND_IMAGE|" \
	-e "s|DEBUG_PLACEHOLDER|$DEBUG_SD|" \
	-e "s|IDLE_DAYS|$IDLE_DAYS|" \
	-e "s|LDAP_BASE|$OPENLDAP_BASE|" \
	-e "s|LDAP_BIND_DN|$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE|" \
	-e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	-e "s|LANG_PLACEHOLDER|$LANG|" \
	-e "s|LDAP_HOST|$OPENLDAP_HOST|" \
	-e "s|LDAP_SCHEME|$OPENLDAP_PROTO|" \
	-e "s|LDAP_START_TLS|$LDAP_START_TLS|" \
	-e "s|LDAP_PORT|$OPENLDAP_PORT|" \
	-e "s|LOGO_IMAGE|$LOGO_IMAGE|" \
	-e "s|USE_CHECKPASSWORD|$USE_CHECKPASSWORD|" \
	-e "s|USE_LOCKACCOUNT|$USE_LOCKACCOUNT|" \
	-e "s|USE_RESETPASSWORD|$USE_RESETPASSWORD|" \
	-e "s|USE_SEARCHEXPIRED|$USE_SEARCHEXPIRED|" \
	-e "s|USE_SEARCHIDLE|$USE_SEARCHIDLE|" \
	-e "s|USE_SEARCHLOCKED|$USE_SEARCHLOCKED|" \
	-e "s|USE_SEARCHWILLEXPIRE|$USE_SEARCHWILLEXPIRE|" \
	-e "s|USE_UNLOCKACCOUNT|$USE_UNLOCKACCOUNT|" \
	-e "s|WILLEXPIRE_DAYS|$WILLEXPIRE_DAYS|" \
	/config.inc.php \
	>/usr/share/service-desk/conf/config.inc.local.php
    chmod 0640 /usr/share/service-desk/conf/config.inc.local.php
fi
if ! test -s /etc/apache2/sites-enabled/service-desk.conf; then
    echo Generates ServiceDesk VirtualHost Configuration
    (
	if test "$AUTH_METHOD" = lemon; then
	    cat <<EOF
PerlOptions +GlobalRequest
PerlModule Lemonldap::NG::Handler::ApacheMP2
EOF
	fi
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
    Alias /favicon.ico /usr/share/service-desk/htdocs/images/favicon.ico
    DirectoryIndex index.php
    DocumentRoot /usr/share/service-desk/htdocs
EOF
	if test "$AUTH_METHOD" = oidc; then
	    if ! echo "$OIDC_CALLBACK_URL" | grep ^/ >/dev/null; then
		OIDC_CALLBACK_URL=/$OIDC_CALLBACK_URL
	    fi
	    CALLBACK_ROOT_SUB_COUNT=`echo $OIDC_CALLBACK_URL | awk -F/ '{print NF}'`
	    CALLBACK_ROOT_SUB_COUNT=`expr $CALLBACK_ROOT_SUB_COUNT - 2 2>/dev/null`
	    if ! test "$CALLBACK_ROOT_SUB_COUNT" -ge 2; then
		CALLBACK_ROOT_SUB_COUNT=2
	    fi
	    CALLBACK_ROOT_URL=`echo $OIDC_CALLBACK_URL | cut -d/ -f 2-$CALLBACK_ROOT_SUB_COUNT`
	    cat <<EOF
    OIDCClientID $OIDC_CLIENT_ID
    OIDCClientSecret $OIDC_CLIENT_SECRET
    OIDCCryptoPassphrase $OIDC_CRYPTO_SECRET
    OIDCProviderMetadataURL $OIDC_PORTAL$OIDC_META_URL
    OIDCProviderTokenEndpointAuth $OIDC_TOKEN_ENDPOINT_AUTH
    OIDCRedirectURI $PUBLIC_PROTO://$APACHE_DOMAIN$OIDC_CALLBACK_URL
    OIDCRemoteUserClaim sub
    RequestHeader set REMOTE_USER expr=%{REMOTE_USER}
    <Location />
	AuthType openid-connect
	Require valid-user
    </Location>
    <Location /$CALLBACK_ROOT_URL/>
	AuthType openid-connect
	Require valid-user
    </Location>
EOF
	    if test "$PING_PATH"; then
		if echo "$PING_PATH" | grep ^/ >/dev/null; then
		    PING_PATH=`echo $PING_PATH | sed 's|^/||'`
		fi
		if ! test -d "$PING_ROOT"; then
		    PING_ROOT=/var/www/html
		fi
		cat <<EOF
    Alias /$PING_PATH $PING_ROOT
    <Location /$PING_PATH/>
	DirectoryIndex index.html
	Require all granted
    </Location>
EOF
	    fi
	elif test "$AUTH_METHOD" = ldap; then
	    cat <<EOF
    <Directory /usr/share/service-desk/htdocs>
	AuthType Basic
	AuthBasicProvider ldap
	AuthLDAPBindDN "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE
	AuthLDAPBindPassword "$OPENLDAP_BIND_PW"
	AuthLDAPURL "$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT/$OPENLDAP_SEARCH" NONE
	AuthName "LDAP Auth"
	AllowOverride None
	Require valid-user
    </Directory>
EOF
	else
	    if test "$AUTH_METHOD" = lemon; then
		cat <<EOF
    PerlHeaderParserHandler Lemonldap::NG::Handler::ApacheMP2
EOF
	    fi
	    cat <<EOF
    <Directory /usr/share/service-desk/htdocs>
	Require all granted
    </Directory>
EOF
	fi
	echo "</VirtualHost>"
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
    if test "$AUTH_METHOD" = oidc; then
	if test "$PUBLIC_PROTO" = https -a "$OIDC_SKIP_TLS_VERIFY"; then
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	else
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	fi
	sed -i \
	    -e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	    -e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	    -e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	    -e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	    -e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	    -e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
    elif test "$AUTH_METHOD" = ldap; then
	export APACHE_IGNORE_OPENLDAP=yay
	rm -f /etc/apache2/mods-enabled/*openid*
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    echo "LDAPVerifyServerCert Off" \
		>>/etc/apache2/sites-enabled/003-vhosts.conf
	fi
    else
	rm -f /etc/apache2/mods-enabled/*openid*
    fi
    if ! test -s /etc/ldap/ldap.conf; then
	if test "$OPENLDAP_PROTO" = ldaps -a "$LDAP_SKIP_TLS_VERIFY"; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	never
EOF
	elif test "$OPENLDAP_PROTO" = ldaps; then
	    cat <<EOF
URI		$OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT
TLS_REQCERT	demand
EOF
	fi >/etc/ldap/ldap.conf
    fi
fi

unset LDAP_START_TLS DEBUG_SD USE_CHECKPASSWORD USE_LOCKACCOUNT PING_PATH \
    USE_RESETPASSWORD USE_UNLOCKACCOUNT OPENLDAP_SEARCH OPENLDAP_FILTER LANG \
    CALLBACK_ROOT_SUB_COUNT OIDC_CALLBACK_URL OIDC_CLIENT_ID PING_ROOT \
    OIDC_CLIENT_SECRET OIDC_CRYPTO_SECRET OIDC_META_URL OIDC_PORTAL \
    OIDC_TOKEN_ENDPOINT_AUTH CALLBACK_ROOT_URL

. /run-apache.sh
